// Copyright Epic Games, Inc. All Rights Reserved.

#include "Week5Tutorial.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Week5Tutorial, "Week5Tutorial" );

DEFINE_LOG_CATEGORY(LogWeek5Tutorial)
 