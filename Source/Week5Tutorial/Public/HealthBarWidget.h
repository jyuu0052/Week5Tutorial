// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "Week5Tutorial/Week5TutorialCharacter.h"
#include "HealthBarWidget.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class WEEK5TUTORIAL_API UHealthBarWidget : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY(meta = (BindWidget))
		class UProgressBar* HealthBar;
	
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* CurrentHealthLabel;

 AWeek5TutorialCharacter* Player;
 void NativeConstruct() override;
 void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

};
