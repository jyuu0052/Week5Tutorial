// Copyright Epic Games, Inc. All Rights Reserved.

#include "Week5TutorialGameMode.h"
#include "Week5TutorialPlayerController.h"
#include "Week5TutorialCharacter.h"
#include "UObject/ConstructorHelpers.h"

AWeek5TutorialGameMode::AWeek5TutorialGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AWeek5TutorialPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}