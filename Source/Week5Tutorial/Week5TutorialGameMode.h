// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Week5TutorialGameMode.generated.h"

UCLASS(minimalapi)
class AWeek5TutorialGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWeek5TutorialGameMode();
};



